#-------------------------------------------------
#
# Project created by QtCreator 2015-05-26T14:20:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KursachKillMePlease
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    TimetableElement.cpp \
    aboutthisprogramm.cpp \
    Route.cpp \
    addroute.cpp \
    deleteroute.cpp \
    findarrivalstation.cpp \
    finddeparturestation.cpp \
    findroutenumber.cpp \
    finddatedeparture.cpp

HEADERS  += mainwindow.h \
    TimetableElement.h \
    aboutthisprogramm.h \
    Route.h \
    addroute.h \
    deleteroute.h \
    findarrivalstation.h \
    finddeparturestation.h \
    findroutenumber.h \
    finddatedeparture.h

FORMS    += mainwindow.ui \
    aboutthisprogramm.ui \
    addroute.ui \
    deleteroute.ui \
    findarrivalstation.ui \
    finddeparturestation.ui \
    findroutenumber.ui \
    finddatedeparture.ui

DISTFILES += \
    Route.txt

RESOURCES += \
    resource.qrc
