#ifndef ROUTE_H_
#define ROUTE_H_

#include <iostream>
#include <fstream>
#include <map>
#include <QWidget>
#include "TimetableElement.h"
#include "ui_mainwindow.h"

class Route {
    public:
    std::map <int, TimetableElement> TimetableMap;//Карта маршрутов
    Route(){};
    ~Route(){};

    void ReadRouteFile();//Чтение маршрута из файла в карту
};


#endif /* ROUT_H_ */
