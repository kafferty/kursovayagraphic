//Окно "О программе"

#ifndef ABOUTTHISPROGRAMM_H
#define ABOUTTHISPROGRAMM_H

#include <QDialog>

namespace Ui {
class aboutthisprogramm;
}

class aboutthisprogramm : public QDialog
{
    Q_OBJECT

public:
    explicit aboutthisprogramm(QWidget *parent = 0);
    ~aboutthisprogramm();

private slots:
    void on_pushButton_clicked();

private:
    Ui::aboutthisprogramm *ui;
};

#endif // ABOUTTHISPROGRAMM_H
