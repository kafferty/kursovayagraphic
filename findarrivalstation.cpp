#include "findarrivalstation.h"
#include "ui_findarrivalstation.h"
#include "TimetableElement.h"
#include <QTableWidget>

FindArrivalStation::FindArrivalStation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindArrivalStation)
{
    ui->setupUi(this);
     QRegExp ST ("[A-Z]{1,1}[a-z]{1,20}");//Проверка ввода
    ui->lineEdit->setValidator(new QRegExpValidator (ST, this));
}

FindArrivalStation::~FindArrivalStation()
{
    delete ui;
}


void FindArrivalStation::on_pushButton_clicked()//Кнопка Find
{
   FindAS();
   accept();

}

std::string FindArrivalStation::FindAS() {//Получение данных от пользователя

    return ui->lineEdit->text().toStdString();
}

void FindArrivalStation::on_pushButton_2_clicked()//Кнопка Cancel
{
    close();
}
