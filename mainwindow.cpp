#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutthisprogramm.h"
#include "addroute.h"
#include "deleteroute.h"
#include "findarrivalstation.h"
#include "TimetableElement.h"
#include "finddeparturestation.h"
#include "findroutenumber.h"
#include "finddatedeparture.h"
#include <map>
#include <string>
#include <QAbstractItemView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(5);
    ROW=0;
    ui->tableWidget->setColumnWidth(4, 150);
    QStringList ist;
    ist<<"Route number" << "Departure station" << "Arrival station" << "Time D and A" << "Date D and A";
    ui->tableWidget->setHorizontalHeaderLabels(ist);
    m = new Route();
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QBrush br(QImage(":/new/img/fon1.jpg"));
    QPalette plt = this->palette();
    plt.setBrush(QPalette::Background, br);
    this->setPalette(plt);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTable() {//Обновление таблицы
    ui->tableWidget->clear();
    QStringList ist;
    ist<<"Route number" << "Departure station" << "Arrival station" << "Time D and A" << "Date D and A";
    ui->tableWidget->setHorizontalHeaderLabels(ist);
    typedef std::map<int, TimetableElement>::const_iterator MapIterator;
    int i = 0;
    m->ReadRouteFile();
    ui->tableWidget->setRowCount(m->TimetableMap.size());
    for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); ++iter, ++i)
    {
        QString StationFrom = QString::fromStdString(iter->second.StationFrom);
        QString StationTo=QString::fromStdString(iter->second.StationTo);
        QString Time = QString::fromStdString(iter->second.Time);
        QString Date = QString::fromStdString(iter->second.Date);
        QString RouteNumber = QString::number(iter->first);
        QTableWidgetItem *itm = new QTableWidgetItem;
        itm->setFlags(itm->flags() & ~Qt::ItemIsEditable);

        itm->setText(RouteNumber);
        ui->tableWidget->setItem(i, 0, itm);

        QTableWidgetItem *itm1 = new QTableWidgetItem();
        itm1->setText(StationFrom);
        ui->tableWidget->setItem(i, 1, itm1);
        QTableWidgetItem *itm2 = new QTableWidgetItem();
        itm2->setText(StationTo);
        ui->tableWidget->setItem(i, 2, itm2);
        QTableWidgetItem *itm3 = new QTableWidgetItem();
        itm3->setText(Time);
        ui->tableWidget->setItem(i, 3, itm3);
        QTableWidgetItem *itm4 = new QTableWidgetItem();
        itm4->setText(Date);
        ui->tableWidget->setItem(i, 4, itm4);
    }
}


void MainWindow::on_actionAbout_this_programm_triggered()
{
    aboutthisprogramm *wnd = new aboutthisprogramm(this);
    wnd->exec();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionAdd_Route_triggered()
{//Добавление маршрута

    addroute *wnd = new addroute(this);
    if(wnd->exec()==addroute::Accepted)
    {
        TimetableElement t;
        t.StationFrom=wnd->getstationfrom().toStdString();
        t.StationTo = wnd->getstationto().toStdString();
        t.RouteNumber = wnd->getroutenumber().toInt();
        t.Date= wnd->getdate().toStdString();
        t.Time= wnd->gettime().toStdString();

        m->TimetableMap.insert(std::pair<int, TimetableElement> (t.RouteNumber, t));
        std::ofstream add;
        add.open("/Users/lidochka/KursachKillMePlease/Route.txt");

        typedef std::map<int, TimetableElement>::const_iterator MapIterator;
        for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); iter++)
        {
            add << iter->second;
        }
        add.close();
       updateTable();
     }
}

void MainWindow::on_pushButton_clicked()
{
    MainWindow::updateTable();
}

void MainWindow::on_pushButton_2_clicked()
{
    aboutthisprogramm *wnd = new aboutthisprogramm(this);
    wnd->show();
}

void MainWindow::on_actionDelete_route_triggered()
{//Удаление маршрута
    deleteroute *wnd = new deleteroute(this);
    if (wnd->exec()==deleteroute::Accepted)
    {
        TimetableElement t;
        t.RouteNumber = wnd->DeleteRoute();
         typedef std::map<int, TimetableElement>::const_iterator MapIterator;
         m->TimetableMap.erase(t.RouteNumber);
         std::ofstream add;
         add.open("/Users/lidochka/KursachKillMePlease/Route.txt");
         typedef std::map<int, TimetableElement>::const_iterator MapIterator;

         for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); iter++)
         {
             add << iter->second;
         }
         add.close();
    }
    updateTable();
}

void MainWindow::on_action_by_arrival_station_triggered()
{//Поиск по станции прибытия
    FindArrivalStation *wnd = new FindArrivalStation(this);
    if (wnd->exec()==FindArrivalStation::Accepted)
    {
        TimetableElement t;
        t.StationTo=wnd->FindAS();
        typedef std::map<int, TimetableElement>::const_iterator MapIterator;
        ui->tableWidget->clear();
        QStringList ist;
        ist<<"Route number" << "Departure station" << "Arrival station" << "Time D and A" << "Date D and A";
        ui->tableWidget->setHorizontalHeaderLabels(ist);
        ui->tableWidget->setRowCount(m->TimetableMap.size());
        int i = 0;
        for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); iter++)
        {


            if ((*iter).second.StationTo == t.StationTo)
            {

                QString StationFrom = QString::fromStdString((*iter).second.StationFrom);
                QString StationTo=QString::fromStdString((*iter).second.StationTo);
                QString Time = QString::fromStdString((*iter).second.Time);
                QString Date = QString::fromStdString((*iter).second.Date);
                QString RouteNumber = QString::number((*iter).first);

                QTableWidgetItem *itm = new QTableWidgetItem;
                itm->setText(RouteNumber);
                ui->tableWidget->setItem(i, 0, itm);

                QTableWidgetItem *itm1 = new QTableWidgetItem();
                itm1->setText(StationFrom);
                ui->tableWidget->setItem(i, 1, itm1);
                QTableWidgetItem *itm2 = new QTableWidgetItem();
                itm2->setText(StationTo);
                ui->tableWidget->setItem(i, 2, itm2);
                QTableWidgetItem *itm3 = new QTableWidgetItem();
                itm3->setText(Time);
                ui->tableWidget->setItem(i, 3, itm3);
                QTableWidgetItem *itm4 = new QTableWidgetItem();
                itm4->setText(Date);
                ui->tableWidget->setItem(i, 4, itm4);
                i++;
            }

        }
    }

}

void MainWindow::on_action_by_departure_station_triggered()
{//Поиск по станции отправления
    FindDepartureStation *wnd = new FindDepartureStation(this);
    if (wnd->exec()==FindDepartureStation::Accepted)
    {
        TimetableElement t;
        t.StationFrom=wnd->FindDS();
        typedef std::map<int, TimetableElement>::const_iterator MapIterator;
        ui->tableWidget->clear();
        QStringList ist;
        ist<<"Route number" << "Departure station" << "Arrival station" << "Time D and A" << "Date D and A";
        ui->tableWidget->setHorizontalHeaderLabels(ist);
        ui->tableWidget->setRowCount(m->TimetableMap.size());
        int i = 0;
        for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); iter++)
        {


            if ((*iter).second.StationFrom == t.StationFrom)
            {
                QString StationFrom = QString::fromStdString((*iter).second.StationFrom);
                QString StationTo=QString::fromStdString((*iter).second.StationTo);
                QString Time = QString::fromStdString((*iter).second.Time);
                QString Date = QString::fromStdString((*iter).second.Date);
                QString RouteNumber = QString::number((*iter).first);

                QTableWidgetItem *itm = new QTableWidgetItem;
                itm->setText(RouteNumber);
                ui->tableWidget->setItem(i, 0, itm);

                QTableWidgetItem *itm1 = new QTableWidgetItem();
                itm1->setText(StationFrom);
                ui->tableWidget->setItem(i, 1, itm1);
                QTableWidgetItem *itm2 = new QTableWidgetItem();
                itm2->setText(StationTo);
                ui->tableWidget->setItem(i, 2, itm2);
                QTableWidgetItem *itm3 = new QTableWidgetItem();
                itm3->setText(Time);
                ui->tableWidget->setItem(i, 3, itm3);
                QTableWidgetItem *itm4 = new QTableWidgetItem();
                itm4->setText(Date);
                ui->tableWidget->setItem(i, 4, itm4);
                i++;
            }

        }
    }

}

void MainWindow::on_action_by_route_number_triggered()
{//Поиск по номеру маршрута
    FindRouteNumber *wnd = new FindRouteNumber(this);
    if (wnd->exec()==FindRouteNumber::Accepted) {
        TimetableElement t;
        t.RouteNumber=wnd->FindRN();
        typedef std::map<int, TimetableElement>::const_iterator MapIterator;
        ui->tableWidget->clear();
        QStringList ist;
        ist<<"Route number" << "Departure station" << "Arrival station" << "Time D and A" << "Date D and A";
        ui->tableWidget->setHorizontalHeaderLabels(ist);
        ui->tableWidget->setRowCount(m->TimetableMap.size());
        int i = 0;
        for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); iter++)
        {
            if ((*iter).first == t.RouteNumber)
            {
                QString StationFrom = QString::fromStdString((*iter).second.StationFrom);
                QString StationTo=QString::fromStdString((*iter).second.StationTo);
                QString Time = QString::fromStdString((*iter).second.Time);
                QString Date = QString::fromStdString((*iter).second.Date);
                QString RouteNumber = QString::number((*iter).first);

                QTableWidgetItem *itm = new QTableWidgetItem;
                itm->setText(RouteNumber);
                ui->tableWidget->setItem(i, 0, itm);

                QTableWidgetItem *itm1 = new QTableWidgetItem();
                itm1->setText(StationFrom);
                ui->tableWidget->setItem(i, 1, itm1);
                QTableWidgetItem *itm2 = new QTableWidgetItem();
                itm2->setText(StationTo);
                ui->tableWidget->setItem(i, 2, itm2);
                QTableWidgetItem *itm3 = new QTableWidgetItem();
                itm3->setText(Time);
                ui->tableWidget->setItem(i, 3, itm3);
                QTableWidgetItem *itm4 = new QTableWidgetItem();
                itm4->setText(Date);
                ui->tableWidget->setItem(i, 4, itm4);
                i++;
            }
        }
    }

}
void MainWindow::on_action_by_departure_date_triggered()
{//Поиск по дате отправления
    FindDateDeparture *wnd = new FindDateDeparture(this);
    if (wnd->exec()==FindDateDeparture::Accepted) {
        TimetableElement t;
        t.Date=wnd->FindDate();
        typedef std::map<int, TimetableElement>::const_iterator MapIterator;
        ui->tableWidget->clear();
        QStringList ist;
        ist<<"Route number" << "Departure station" << "Arrival station" << "Time D and A" << "Date D and A";
        ui->tableWidget->setHorizontalHeaderLabels(ist);
        ui->tableWidget->setRowCount(m->TimetableMap.size());
        int i = 0;
        for (MapIterator iter = m->TimetableMap.begin(); iter!=m->TimetableMap.end(); iter++)
        {
            int k;
            k = (*iter).second.Date.find(t.Date);
            if (k==0)
            {
                QString StationFrom = QString::fromStdString((*iter).second.StationFrom);
                QString StationTo=QString::fromStdString((*iter).second.StationTo);
                QString Time = QString::fromStdString((*iter).second.Time);
                QString Date = QString::fromStdString((*iter).second.Date);
                QString RouteNumber = QString::number((*iter).first);

                QTableWidgetItem *itm = new QTableWidgetItem;
                itm->setText(RouteNumber);
                ui->tableWidget->setItem(i, 0, itm);

                QTableWidgetItem *itm1 = new QTableWidgetItem();
                itm1->setText(StationFrom);
                ui->tableWidget->setItem(i, 1, itm1);
                QTableWidgetItem *itm2 = new QTableWidgetItem();
                itm2->setText(StationTo);
                ui->tableWidget->setItem(i, 2, itm2);
                QTableWidgetItem *itm3 = new QTableWidgetItem();
                itm3->setText(Time);
                ui->tableWidget->setItem(i, 3, itm3);
                QTableWidgetItem *itm4 = new QTableWidgetItem();
                itm4->setText(Date);
                ui->tableWidget->setItem(i, 4, itm4);
                i++;
            }

        }
    }

}

