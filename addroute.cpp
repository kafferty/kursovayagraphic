#include "addroute.h"
#include <iostream>
#include "ui_addroute.h"
#include "TimetableElement.h"
#include <map>
#include <QRegExp>
#include <QRegExpValidator>
#include "Route.h"

addroute::addroute(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addroute)
{
    ui->setupUi(this);//Проверка ввода
    QRegExp RN ("[0-9]{1,3}");
    QRegExp SF ("[A-Z]{1,1}[a-z]{1,20}");
    QRegExp ST ("[A-Z]{1,1}[a-z]{1,20}");
    QRegExp T ("[0-2]{1,1}[0-9]{1,1}:{1,1}[0-5]{1,1}[0-9]{1,1}-{1,1}[0-2]{1,1}[0-9]{1,1}:{1,1}[0-5]{1,1}[0-9]{1,1} ");
    QRegExp D ("[0-3]{1,1}[0-9]{1,1}.{1,1}[0-1]{1,1}[0-2]{1,1}.{1,1}[1-9]{2,2}-{1,1}[0-3]{1,1}[0-9]{0,1}.{1,1}[0-1]{1,1}[0-2]{1,1}.{1,1}[1-9]{2,2}");
    ui->lineEdit->setValidator(new QRegExpValidator (RN, this));
    ui->lineEdit_2->setValidator(new QRegExpValidator (SF, this));
    ui->lineEdit_3->setValidator(new QRegExpValidator (ST, this));
    ui->lineEdit_4->setValidator(new QRegExpValidator (T, this));
    ui->lineEdit_5->setValidator(new QRegExpValidator (D, this));
}

addroute::~addroute()
{
    delete ui;
}

void addroute::on_pushButton_2_clicked()//Кнопка Cancel
{
    close();
}

void addroute:: EntryRouteFile()//Получение данных от пользователя
{
    RouteNumber=ui->lineEdit->text();
    StationFrom=ui->lineEdit_2->text();
    StationTo=ui->lineEdit_3->text();
    Time=ui->lineEdit_4->text();
    Date=ui->lineEdit_5->text();
}


void addroute::on_pushButton_clicked()//Кнопка ADD
{
    EntryRouteFile();
    accept();
}
