#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Route.h"
#include <iostream>
#include <fstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void updateTable();

private slots:
    void on_actionAbout_this_programm_triggered();
    void on_actionAdd_Route_triggered();
    void on_actionExit_triggered();
    void on_pushButton_2_clicked();//Кнопка About
    void on_pushButton_clicked();//Кнопка Show Table
    void on_actionDelete_route_triggered();
    void on_action_by_arrival_station_triggered();
    void on_action_by_departure_station_triggered();
    void on_action_by_route_number_triggered();
    void on_action_by_departure_date_triggered();

private:
    Ui::MainWindow *ui;
    int ROW;
    Route *m;
};

#endif // MAINWINDOW_H
