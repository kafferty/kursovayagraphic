#include "deleteroute.h"
#include "ui_deleteroute.h"
#include "TimetableElement.h"
#include "mainwindow.h"
#include "Route.h"
#include <QRegExp>

deleteroute::deleteroute(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::deleteroute)
{
    ui->setupUi(this);
    QRegExp RN ("[0-9]{1,3}");
    ui->lineEdit->setValidator(new QRegExpValidator (RN, this));//Проверка ввода
}

deleteroute::~deleteroute()
{
    delete ui;
}

int deleteroute::DeleteRoute()//Получение данных от пользователя
{
    return ui->lineEdit->text().toInt();
}

void deleteroute::on_pushButton_clicked()//Кнопка Delete
{
    DeleteRoute();
    accept();
}

void deleteroute::on_pushButton_2_clicked()//Кнопка Cancel
{
    close();
}
