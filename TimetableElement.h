#ifndef TIMETABLEELEMENT_H
#define TIMETABLEELEMENT_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


class TimetableElement { //Структура Расписание
public:
    std::string StationFrom;//Станция отправления
    std::string StationTo;//Станция прибытия
    int RouteNumber;//Номер маршрута
    std::string Time;//Время
    std::string Date;//Дата
    friend std::ofstream &operator <<(std::ofstream &add, const TimetableElement &z);

    friend std::ifstream &operator >>(std::ifstream &get, TimetableElement &z);

    friend std::ostream &operator <<(std::ostream &add, const TimetableElement &z);

    friend std::istream &operator >>(std::istream &get, TimetableElement &z);

    TimetableElement operator = (const TimetableElement &z);

    TimetableElement (const TimetableElement &z);

    TimetableElement (){};
    ~TimetableElement (){};

};
#endif
