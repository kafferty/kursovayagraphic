#include "finddatedeparture.h"
#include "ui_finddatedeparture.h"

FindDateDeparture::FindDateDeparture(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindDateDeparture)
{
    ui->setupUi(this);
    QRegExp D ("[1-3]{1,1}[0-9]{0,1}.{1,1}[0-1]{1,1}[0-2]{1,1}.{1,1}[1-9]{2,2}");
    ui->lineEdit->setValidator(new QRegExpValidator (D, this));
}

FindDateDeparture::~FindDateDeparture()
{
    delete ui;
}


void FindDateDeparture::on_pushButton_clicked()//Кнопка Find
{
    FindDate();
    accept();
}

std::string FindDateDeparture::FindDate()//Получение данных от пользователя
{
    return ui->lineEdit->text().toStdString();
}

void FindDateDeparture::on_pushButton_2_clicked()//Кнопка Cancel
{
    close();
}
