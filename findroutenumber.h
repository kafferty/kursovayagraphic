//Окно поиска по номеру маршрута

#ifndef FINDROUTENUMBER_H
#define FINDROUTENUMBER_H

#include <QDialog>

namespace Ui {
class FindRouteNumber;
}

class FindRouteNumber : public QDialog
{
    Q_OBJECT
    friend class MainWindow;

public:
    explicit FindRouteNumber(QWidget *parent = 0);
    ~FindRouteNumber();
    QString RouteNumber;

private slots:
    void on_pushButton_clicked();
    int FindRN();
    void on_pushButton_2_clicked();

private:
    Ui::FindRouteNumber *ui;
};

#endif // FINDROUTENUMBER_H
