//Окно "Удаление маршрута"

#ifndef DELETEROUTE_H
#define DELETEROUTE_H

#include <QDialog>

namespace Ui {
class deleteroute;
}

class deleteroute : public QDialog
{
    Q_OBJECT

    friend class MainWindow;

public:
    explicit deleteroute(QWidget *parent = 0);
    ~deleteroute();
    QString RouteNumber;

private slots:
    void on_pushButton_clicked();
    int DeleteRoute();

    void on_pushButton_2_clicked();

private:
    Ui::deleteroute *ui;
};

#endif // DELETEROUTE_H
