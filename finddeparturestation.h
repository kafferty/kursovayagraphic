//Окно поиска по станции отправления

#ifndef FINDDEPARTURESTATION_H
#define FINDDEPARTURESTATION_H

#include <QDialog>
#include "Route.h"
#include "TimetableElement.h"
namespace Ui {
class FindDepartureStation;
}

class FindDepartureStation : public QDialog
{
    Q_OBJECT
    friend class MainWindow;
public:
    explicit FindDepartureStation(QWidget *parent = 0);
    ~FindDepartureStation();
    QString StationFrom;

private slots:
    void on_pushButton_clicked();
    std::string FindDS();
    void on_pushButton_2_clicked();

private:
    Ui::FindDepartureStation *ui;
};

#endif // FINDDEPARTURESTATION_H
