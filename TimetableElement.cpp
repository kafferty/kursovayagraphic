#include "TimetableElement.h"
#include <iomanip>

using namespace std;


ofstream &operator <<(ofstream &add, const TimetableElement &z)
{//Перегрузка оператора записи для Timetable
    add<<z.RouteNumber<<" ";
    add<<z.StationFrom<<" ";
    add<<z.StationTo<<" ";
    add<<z.Date<<" ";
    add<<z.Time;
    add<<endl;
    return add;
}


ifstream &operator >>(ifstream &get, TimetableElement &z)
{//Перегрузка оператора чтения для TimetableElement
    get>>z.RouteNumber;
    get>>z.StationFrom;
    get>>z.StationTo;
    get>>z.Date;
    get>>z.Time;
    return get;
}


bool operator == (const TimetableElement &date, const TimetableElement &date2) //Оператор сравнения
{
    if (date.Date==date2.Date) {
        return true;
    }
    return false;
}



TimetableElement TimetableElement::operator = (const TimetableElement &z)//Оператор присваивания для TimetableElement
{
    StationFrom = z.StationFrom;
    StationTo = z.StationTo;
    RouteNumber = z.RouteNumber;
    Date = z.Date;
    Time = z.Time;
    return z;
}

TimetableElement::TimetableElement (const TimetableElement &z)//Оператор копирования для TimetableElement
{
    RouteNumber=z.RouteNumber;
    StationFrom = z.StationFrom;
    StationTo = z.StationTo;
    RouteNumber = z.RouteNumber;
    Date = z.Date;
    Time = z.Time;
}


