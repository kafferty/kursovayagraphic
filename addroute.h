//Окно "Добавление маршрута"

#ifndef ADDROUTE_H
#define ADDROUTE_H

#include <QDialog>

namespace Ui {
class addroute;
}

class addroute : public QDialog
{
    Q_OBJECT

public:
    explicit addroute(QWidget *parent = 0);
    ~addroute();
    const QString getstationfrom()
    {
        return StationFrom;
    }
    const QString getstationto()
    {
        return StationTo;
    }
    const QString getroutenumber()
    {
        return RouteNumber;
    }
    const QString gettime()
    {
        return Time;
    }
    const QString getdate()
    {
        return Date;
    }

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();
    void EntryRouteFile();

private:
    Ui::addroute *ui;
    QString StationFrom;
    QString StationTo;
    QString RouteNumber;
    QString Date;
    QString Time;

};

#endif // ADDROUTE_H
