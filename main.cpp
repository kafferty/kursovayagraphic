//Городничева Лидия, 13501/3
//Курсовая работа "Редактор расписания поездов"

#include "mainwindow.h"
#include <QApplication>
#include "Route.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    Route t;
    t.ReadRouteFile();
    w.show();//Показ MainWindow

    return a.exec();
}
