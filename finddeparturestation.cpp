#include "finddeparturestation.h"
#include "ui_finddeparturestation.h"
#include "mainwindow.h"

FindDepartureStation::FindDepartureStation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindDepartureStation)
{
    ui->setupUi(this);
    QRegExp SF ("[A-Z]{1,1}[a-z]{1,20}");
    ui->lineEdit->setValidator(new QRegExpValidator (SF, this));
}

FindDepartureStation::~FindDepartureStation()
{
    delete ui;
}

void FindDepartureStation::on_pushButton_clicked()//Кнопка Find
{
    FindDS();
    accept();
}

std::string FindDepartureStation::FindDS()//Получение данных от пользоватея
{
   return ui->lineEdit->text().toStdString();
}

void FindDepartureStation::on_pushButton_2_clicked()//Кнопка Cancel
{
    close();
}
