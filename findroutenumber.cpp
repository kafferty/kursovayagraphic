#include "findroutenumber.h"
#include "ui_findroutenumber.h"
#include "mainwindow.h"
#include "TimetableElement.h"
#include "Route.h"
#include <QTableWidget>

FindRouteNumber::FindRouteNumber(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindRouteNumber)
{
    ui->setupUi(this);
    QRegExp RN ("[0-9]{1,3}");
    ui->lineEdit->setValidator(new QRegExpValidator (RN, this));
}

FindRouteNumber::~FindRouteNumber()
{
    delete ui;
}

void FindRouteNumber::on_pushButton_clicked()//Кнопка Find
{
    FindRN();
    accept();
}

int FindRouteNumber::FindRN()//Получение данных от пользователя
{
    return ui->lineEdit->text().toInt();
}


void FindRouteNumber::on_pushButton_2_clicked()//Кнопка Cancel
{
    close();
}
