//Окно поиска маршрута по станции прибытия

#ifndef FINDARRIVALSTATION_H
#define FINDARRIVALSTATION_H

#include <QDialog>
#include <QString>

namespace Ui {
class FindArrivalStation;
}

class FindArrivalStation : public QDialog
{
    Q_OBJECT

    friend class MainWindow;
public:
    explicit FindArrivalStation(QWidget *parent = 0);
    ~FindArrivalStation();
    QString StationTo;

private slots:
    void on_pushButton_clicked();
    std::string FindAS();
    void on_pushButton_2_clicked();

private:
    Ui::FindArrivalStation *ui;
};

#endif // FINDARRIVALSTATION_H
