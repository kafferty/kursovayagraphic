//Окно поиска по дате отправления

#ifndef FINDDATEDEPARTURE_H
#define FINDDATEDEPARTURE_H

#include <QDialog>

namespace Ui {
class FindDateDeparture;
}

class FindDateDeparture : public QDialog
{
    Q_OBJECT
    friend class MainWindow;

public:
    explicit FindDateDeparture(QWidget *parent = 0);
    ~FindDateDeparture();
    QString Date;

private slots:
    void on_pushButton_clicked();
    std::string FindDate();
    void on_pushButton_2_clicked();

private:
    Ui::FindDateDeparture *ui;
};

#endif // FINDDATEDEPARTURE_H
